import string
import hashlib
import hmac
import requests
import json
from threading import Thread
import time
import datetime

##################################################################################
ID = "ZOZ0WD80"
CTIME = "1611413629"
SONG_PATH = "/api/v2/song/getInfo"
STREAM_PATH = "/api/v2/song/getStreaming"
LYRIC_PATH = "/api/v2/lyric"
PLAYLIST_PATH = "/api/v2/playlist/getDetail"
LIST_ALBUM_PATH = "/api/v2/album/getList"
KEY = "882QcNXV4tUZbvAsjmFOHqNC1LpcBRKW"
API_KEY = "kI44ARvPwaqL7v0KuDSM0rGORtdY1nnw"
START = 0
END = 54000000
STEP = 500
COOKIE = "_ga=GA1.2.1726507564.1614649693; fuid=babda76611ae510629630d3db791f062; zmp3_app_version.1=119; _zlang=vn; _gid=GA1.2.873265140.1619661830; __zi=3000.SSZzejyD0jSbZUgxWaGPoJIFlgNCIW6AQ9sqkju84vnwakgnsq1ScdUVuBQVILERCfZWjfzCNP8rD0.1; adtimaUserId=3000.SSZzejyD0jSbZUgxWaGPoJIFlgNCIW6AQ9sqkju84vnwakgnsq1ScdUVuBQVILERCfZWjfzCNP8rD0.1; fpsend=149969; atmpv=1; _gat=1"
##################################################################################


def Hash256(value):
    h = hashlib.sha256(value.encode('utf-8'))
    return h.hexdigest()


def Hash512(value, key):
    return hmac.new(key.encode('utf-8'), value.encode('utf-8'), hashlib.sha512).hexdigest()


def getSongUrl(id, ctime):
    sig = Hash512(SONG_PATH + Hash256("ctime=" + ctime +
                                      "id=" + id + "version=1.0.19"), KEY)
    return "https://zingmp3.vn/api/v2/song/getInfo?id=" + id + "&ctime=" + ctime + "&version=1.0.19&sig=" + sig + "&apiKey=" + API_KEY


def getLyricUrl(id, ctime):
    sig = Hash512(LYRIC_PATH + Hash256("ctime=" + ctime +
                                       "id=" + id + "version=1.0.19"), KEY)
    return "https://zingmp3.vn/api/v2/song/getStreaming?id=" + id + "&ctime=" + ctime + "&version=1.0.19&sig=" + sig + "&apiKey=" + API_KEY


def getPlaylistUrl(id, ctime):
    sig = Hash512(PLAYLIST_PATH + Hash256("ctime=" + ctime +
                                          "id=" + id + "version=1.0.19"), KEY)
    return "https://zingmp3.vn/api/v2/playlist/getDetail?id=" + id + "&ctime=" + ctime + "&version=1.0.19&sig=" + sig + "&apiKey=" + API_KEY


t = "sort=listencount=1page=1type=genre"
t2 = "type=genrepage=1count=1sort=listen"
t3 = "type=genre&page=1&count=18&sort=listen"


def getListAlbumcUrl(id, ctime):
    s = "count=500"+"ctime=" + ctime+"id=" + id + \
        "page=1"+"type=genre" + "version=1.0.19"
    print(s)
    sig = Hash512(LIST_ALBUM_PATH + Hash256(s), KEY)
    return "https://zingmp3.vn/api/v2/album/getList?"+"ctime=" + ctime + "&type=genre"+"&page=1"+"&count=500"+"&sort=listen"+"&id=" + id + "&version=1.0.19&sig=" + sig + "&apiKey=" + API_KEY

##################################################################################


def writeData(path, data):
    f = open(path, 'a+', encoding='utf-8')
    f.write(data+"\n")


def writeError(path, data):
    f = open(path, 'a+', encoding='utf-8')
    obj = json.dumps(data, ensure_ascii=False).encode("utf8")
    f.write(obj.decode()+"\n")


def writeTotal(data):
    f = open("total.txt", 'w', encoding='utf-8')
    f.write(str(data))


def getStart():
    f = open("total.txt")
    return int(f.read())

def CloneAlbum(objType):
    listSong = {}
    url = getListAlbumcUrl(objType['id'], CTIME)
    r = requests.get(url)
    cook = r.headers["Set-Cookie"]
    print(url)
    r = requests.get(url,headers={"cookie":cook})
    album = r.json()
    #print(album)
    if album['err'] == 0:
        for pl in album['data']['items']:
            urlPL = getPlaylistUrl(pl['encodeId'],CTIME)
            resPl = requests.get(urlPL,headers={"cookie":cook}).json()
            try:
                for song in resPl['data']['song']['items']:
                    if song['encodeId'] not in listSong:
                        listSong[song['encodeId']] = True
                        writeData(objType['name']+".txt",song['encodeId'])
            except:
                print("err")

listType = [
    {id: "IWZ9Z088", 'name': 'VN_Nhac_Tre_VPop'},
    {id: "IWZ9Z08C", 'name': 'VN_Nhac_Cach_Mang'},
    {id: "IWZ9Z08D", 'name': 'VN_Nhac_Dan_Ca'},
    {id: "IWZ9Z08E", 'name': 'VN_Nhac_Trinh'},
    {id: "IWZ9Z0C6", 'name': 'VN_Nhac_Cai_Luong'},
    {id: "IWZ9Z090", 'name': 'VN_Nhac_Khong_Loi'},
    {id: "IWZ9Z08A", 'name': 'VN_Nhac_Rock_Viet'},
    {id: "IWZ9Z0CW", 'name': 'VN_Nhac_Dance'},
    {id: "IWZ9Z089", 'name': 'VN_Nhac_Rap_Viet'},
    {id: "IWZ97FCE", 'name': 'VN_Nhac_EDM_Viet'},
    {id: "IWZ9Z08F", 'name': 'VN_Nhac_Thieu_NHi'},
    {id: "IWZ9Z0FZ", 'name': 'VN_Nhac_Ton_Giao'},
    
    {id: "IWZ9Z099", 'name': 'US_UK_Rock'},
    {id: "IWZ9Z09B", 'name': 'US_UK_Rap_Hip_Hop'},
    {id: "IWZ9Z09C", 'name': 'US_UK_Blue_Jazz'},
    {id: "IWZ9Z097", 'name': 'US_UK_Pop'},
    {id: "IWZ9Z09E", 'name': 'US_UK_Folk'},
    {id: "IWZ97797", 'name': 'US_UK_Latin'},
    {id: "IWZ9Z096", 'name': 'US_UK_Country'},
    {id: "IWZ980O7", 'name': 'US_UK_Classical'},
    {id: "IWZ9Z0F6", 'name': 'US_UK_Reggae'},
    {id: "IWZ9ZIUO", 'name': 'US_UK_Alternative'},
    {id: "IWZ9Z0CA", 'name': 'US_UK_Indie'},
]


##################################################################################
if __name__ == "__main__":
    #print(getSongUrl('ZW7ODZZU',CTIME))
    CloneAlbum({'id': "IWZ9Z089", 'name': 'VN_Nhac_Rap_Viet'})

